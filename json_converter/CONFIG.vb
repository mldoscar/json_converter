﻿Module CONFIG

    ''VARIABLE PRIVADA PARA LA RUTA DE LA API EN LA WEB
    Private _api_host As String = "http://localhost/api.php"

    ''PROPIEDAD DE LA VARIABLE PRIVADA DE LA API
    Public Property API_HOST() As String
        Get
            Return _api_host
        End Get
        Set(ByVal value As String)
            _api_host = value
        End Set
    End Property

End Module
