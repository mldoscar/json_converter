﻿'*********************************************
'Copyright 2015
'Author: Oscar Eduardo Rodriguez Largaespada
'Contact: admin@nicacode.com
'*********************************************

Imports System.Net
Imports System.IO
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq


Public Class Form_Main

    Sub New()
        InitializeComponent()

        Me.Text = "Jalar un json"

        ''Asignación del cuadro de texto
        Me.Host.Text = API_HOST
    End Sub

    Private Sub Action_Click(sender As Object, e As EventArgs) Handles Action.Click
        'Dim POST_DATA As String
        'Dim POST_DATA_BYTES As Byte()
        Dim GET_TEXT As String

        Dim solicitud As HttpWebRequest
        Dim respuesta As HttpWebResponse
        Dim datos As Stream
        Dim traduccion As StreamReader

        solicitud = HttpWebRequest.Create(Host.Text.Trim)
        With solicitud
            '' Establece Tipo de Autenticación
            .Credentials = CredentialCache.DefaultCredentials

            ' ''Método de solicitud http
            .Method = "GET"
            '.Method = "POST"

            ' ''Valores del método POST
            'POST_DATA = "parametro1=valorparametro1&parametro2=valorparametro2"

            ' ''Conversión a Bytes de los valores
            'POST_DATA_BYTES = System.Text.Encoding.UTF8.GetBytes(POST_DATA)

            ' ''Establecer el Tipo de Contenido en el header
            '.ContentType = "application/x-www-form-urlencoded"

            ' ''Establecer la longitud de los datos (del array de bytes)
            '.ContentLength = POST_DATA.Length

            ' ''Crear el Escritor para POSTEAR los bytes
            'Using DATA_STREAM As System.IO.Stream = solicitud.GetRequestStream
            '    DATA_STREAM.Write(POST_DATA_BYTES, 0, POST_DATA_BYTES.Length)
            '    DATA_STREAM.Close()
            'End Using
            ' ''Se liberan todos los recursos utilizados

        End With

        ''Solicitar una respuesta de la solicitud
        respuesta = solicitud.GetResponse
        ''Crear un lector de Respuesta
        datos = respuesta.GetResponseStream
        traduccion = New System.IO.StreamReader(datos)

        ''Almacenar los datos del lector en una variable de tipo Cadena
        GET_TEXT = traduccion.ReadToEnd

        ''Validar el Json
        If ValidarJson(GET_TEXT) Then
            ''Convertir el Json (des-serializar a DataTable)
            Dim valores As Object = ConvertirJson(GET_TEXT)
            ''Asignar el Objeto con los datos al DataGrid para mostrar los datos
            GridMain.DataSource = valores
        End If
        '' Fin del payaso
    End Sub

    ''Función que valida si es un Json Válido
    ''Puede variar según el tipo de valores de Json que se manden
    ''Serialización para objetos: JObject.Parse(arg)
    ''Serialización para tablas o arreglos de dos dimensiones: JArray.Parse(args)
    Private Function ValidarJson(jsonString As String) As Boolean
        Try
            Dim jo As JArray = JArray.Parse(jsonString)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    ''Función que convierte el Json a un DataTable
    Private Function ConvertirJson(jsonString As String) As Object
        Dim ob As Object = Nothing
        Try
            Dim tabla As DataTable = CType(
            JsonConvert.DeserializeObject(jsonString), 
            JArray
            ).ToObject(Of DataTable)()
            ob = tabla
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return ob

    End Function


End Class
