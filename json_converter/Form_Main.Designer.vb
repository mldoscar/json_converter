﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Main
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Host = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Action = New System.Windows.Forms.Button()
        Me.GridMain = New System.Windows.Forms.DataGridView()
        CType(Me.GridMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Host
        '
        Me.Host.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Host.Location = New System.Drawing.Point(45, 10)
        Me.Host.Name = "Host"
        Me.Host.Size = New System.Drawing.Size(724, 20)
        Me.Host.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "host"
        '
        'Action
        '
        Me.Action.Location = New System.Drawing.Point(775, 8)
        Me.Action.Name = "Action"
        Me.Action.Size = New System.Drawing.Size(75, 23)
        Me.Action.TabIndex = 2
        Me.Action.Text = "GET!"
        Me.Action.UseVisualStyleBackColor = True
        '
        'GridMain
        '
        Me.GridMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridMain.Location = New System.Drawing.Point(12, 37)
        Me.GridMain.Name = "GridMain"
        Me.GridMain.Size = New System.Drawing.Size(838, 425)
        Me.GridMain.TabIndex = 3
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(862, 474)
        Me.Controls.Add(Me.GridMain)
        Me.Controls.Add(Me.Action)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Host)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.GridMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Host As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Action As System.Windows.Forms.Button
    Friend WithEvents GridMain As System.Windows.Forms.DataGridView

    Private Sub Form_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        Action.Location = New Point(Host.Location.X + Host.Width + 5, Action.Location.Y)
    End Sub

    Private Sub DataGridView_DataSourceChanged(sender As Object, e As EventArgs) Handles GridMain.DataSourceChanged
        For Each col As DataGridViewColumn In GridMain.Columns
            col.Width = col.GetPreferredWidth(DataGridViewAutoSizeColumnMode.AllCells, False)
        Next
    End Sub
End Class
