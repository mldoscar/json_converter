# README #

Para probar el código debes hacer lo siguiente

### Requisitos previos ###

* Servidor Web, local o remoto
* .NET Framework 4.0 Client Profile o superior
* IDE de desarrollo para Visual Basic .NET (2012 o superior)

### Configuración ###

* Colocar el archivo 'api.php' en un servidor web
* Verificar que la referencia del la libreria 'Newtonsoft.Json.dll' esté agregada en el proyecto de Visual Studio (El archivo está ubicado en 'json_converter\Newtonsoft.Json.dll')
* Cambiar el Valor de la Propiedad API_HOST ubicada en el módulo 'json_converter\CONFIG.vb'
* Compilar
* Divertirse



```
#!vb.net
'Copyright (c) 2015
'Author: Oscar Eduardo Rodriguez Largaespada
'Contact: mld.oscar@nicacode.com
'Youre always Welcome
```